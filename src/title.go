// this file contains data structure definitions for the title descriptor data
// structure.

package main

// a collectable node in the region / chapter
// the parent is a region
type TitleCollectable struct {
    ID int      // ID referencingthe TitleC in global scope
    Max int     // the number of this collectable in the parent-Region
}

// split a region / chapter in subregions if necessary
type TitleSubRegion struct {
    Name string                         // name of the subregion
    Number int                          // the number of the subregion (e.g. 3.4 if region.Number = 3, this.Number=4)
    Collectable []TitleCollectable      // list of collectables to be found in this subregion
}

// a region / chapter;
type TitleRegion struct {
    Name string                         // the name of that region or chapter
    Number int                          // the number (if they are counted)
    SubRegion []TitleSubRegion          // a list of subregions; if []: this.Collectable will be used
    Collectable []TitleCollectable      // list of collectables to be found in this region; only used if this.SubRegion is empty
}

// global collectable identifier
type TitleC struct {
    ID int                              // an ID, must be unique
    Shortcut string                     // a shortcut for naming of the titles (won't be visible)
    Name string                         // the name of this collectable
    Pattern string                      // placeholder pattern in the title
    TotalNumber int                     // if positive denotes the total number of this collectable
}

// root element of the Title-data structure
type Title struct {
    Region []TitleRegion                // list of regions with collectables in the game
    RegionPattern string                // placeholder pattern for the region name in the title
    SubRegionPattern string             // placeholder pattern for the subregion name in the title
    Collectable []TitleC                // global descriptor of available collectable types
    CalculateOffset bool
    Video []Video
}

type Video struct {
    Chapter int
    Name string
    Offset string
    Cutting []Zone
}

type Zone struct {
    Name string

    Time string
    Before uint
    BeforeTime string
    After uint
    AfterTime string

    In string
    Out string
}
