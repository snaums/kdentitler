// KDEntitler takes a title description (JSON) and a KDEnlive project file.
// It is intended for automatically generating titles for achievement-guide
// (find all collectables in a video game) type videos.
//
// (1) It will identify a template title to be used
// (2) It will create new titles for every region and collectable instance therein
//     - if there are any for subregions and their collectable instances
// (3) It will write a new KDEnlive project with that additional information
//
// Author: snaums (me@snaums.de)


package main

import (
    "os"
    "fmt"
    "flag"
    "io/ioutil"
    "strings"
    "strconv"
    "errors"
    "codeberg.org/snaums/kdentitler/xmldom"
    "github.com/gookit/color"
    "encoding/json"
)

// a list of tested version strings; if you test this program with other versions,
// I'm happy to accept your pull request or Email me
var KDEnliveTestedVersions []string = []string{
    "19.12.1",
    "19.12.2",
    "19.12.3",
};

type ProducerClipzone struct {
    In uint      `json:"in"`          // start marker (in frames!)
    Name string  `json:"name"`        // name (probably anything)
    Out uint     `json:"out"`         // out marker (in frames!)
}

// representation of a timestamp in a video
type VideoLength struct {
    hrs uint                        // hour
    min uint                        // minute
    sec uint                        // second
    frs uint                        // frame
}

// representation of a producer (video) in the KDEnlive xml-file
type ProducerVideo struct {
    Node *xmldom.Node               // link to the xnml node

    Id string                       // kdenlive:id
    Path string                     // kdenlive:path
    Filename string                 // filename 
    ClipName string                 // internal clip name in kdenlive

    In string                       // In timestamp
    InLength VideoLength            // In timestamp
    Out string                      // Out timestamp
    OutLength VideoLength           // Out timestamp

    VideoIndex int
    AudioIndex int

    FrameRate float64               // calculated from meta.media.frame_rate_num / frame_rate_den
    KdenliveID int                  // kdenlive:id

    ClipZones []ProducerClipzone    // a list of clip zones to be flushed to the xml
    FolderName string               // name of the containing folder
    FolderId int                    // id of the containing folder
}

// read the second field of a timestamp with frames (secs,frames)
// \param fmt seconds field
// \return seconds, frames, errorcode if any
func readSecTime ( fmt string ) ( int, int, error ) {
    sp := strings.Split ( fmt, "," );
    sp2 := strings.Split ( fmt, "." );
    if len(sp2) > len(sp) {
        sp = sp2;
    }
    if len(sp) == 1 {
        s, err := strconv.Atoi ( sp[0] );
        return s, 0, err
    } else if len(sp) == 2 {
        s, err := strconv.Atoi ( sp[0] );
        if err != nil {
            return 0,0, err
        }
        f, err := strconv.Atoi ( sp[1] );
        return s, f, err
    }
    return 0,0, errors.New ("Format not recognized");
}

// read a timestamp (hrs:min:sec,frames)
// \param fmt input string
// \return the parsed timestamp, errorcode if any
func readLengthTime ( fmt string ) ( VideoLength, error ) {
    var err error = nil
    var h,m,s,f int
    var v VideoLength
    s1 := strings.Split ( fmt, ":" );
    s2 := strings.Split ( fmt, "-" );
    s3 := strings.Split ( fmt, "." );
    var splitted []string

    if len(s1) > len(s2) && len(s1) > len(s3) && len(s1) <= 3 {
        splitted = s1;
    } else if len(s2) > len(s3) && len(s2) <= 3 {
        splitted = s2
    } else {
        splitted = s3
    }

    if len(splitted) == 3 {
        h, err = strconv.Atoi ( splitted[0] );
        if err != nil {
            return v, err
        }
        m, err = strconv.Atoi ( splitted[1] );
        if err != nil {
            return v, err
        }

        s,f, err = readSecTime ( splitted[2] );
        if err != nil {
            return v, err
        }
    } else if len(splitted) == 2 {
        h = 0
        m, err = strconv.Atoi ( splitted[0] );
        if err != nil {
            return v, err
        }

        s,f, err = readSecTime ( splitted[1] );
        if err != nil {
            return v, err
        }
    } else if len(splitted) == 1 {
        h = 0;
        m = 0;
        s,f, err = readSecTime ( splitted[0] );
        if err != nil {
            return v, err
        }
    } else {
        PrintNote ("Split times seem weird! Original time: ", fmt );
    }
    var vl VideoLength = VideoLength{ uint(h), uint(m), uint(s), uint(f) };
    return vl, err
}

// debug function for printing the video timestamp
func printLengthTime ( vl VideoLength ) string {
    return fmt.Sprintf ( "%2d:%2d:%2d,%3d", vl.hrs, vl.min, vl.sec, vl.frs );
}

// is a video already in the list?
// \param pr list of videos
// \param name name of the new video
// \param path path of the new video
// \return true or false
func testVideoInVidList ( pr []ProducerVideo, name string, path string ) bool {
    for _, v := range pr {
        if len (v.ClipName ) > 0 && v.ClipName == name {
            return true
        }
        if v.Path == path {
            return true;
        }
    }
    return false;
}

// reads a single property off a xml-node
// \param node reference to the queried xml node
// \param prop property to query
// \return value of the property or error code
func readSingleProperty ( node *xmldom.Node, prop string ) ( string, error ) {
    n := node.QueryOne ( "property[@name='" + prop +"']")
    if n != nil {
        return n.Text, nil;
    }
    return "", errors.New ("property " + prop + " not found");
}

// string to int
// \param v input string
// \param err error from a previous other call in the code
// \return see strconv.Atoi
func toInt ( v string, err error ) ( int, error ) {
    if err != nil {
        return -1, err
    }
    return strconv.Atoi ( v );
}

// find the name of a folder, given an id
// \param doc reference to the xmldocument
// \param folderID the id of the folder being queried
// \return foldername or errorcode
func FindFolderName ( doc *xmldom.Document, folderID int ) ( string, error ) {
    root := doc.Root
    node := root.QueryOne ("playlist[@id='main_bin']/property[@name='kdenlive:folder.-1."+strconv.Itoa ( folderID )+"']");
    if node != nil {
        return node.Text, nil
    } else {
        return "", errors.New ("Folder node not found");
    }
}

// read a single producer of the xmldocument
// \param node xml-node to be parsed
// \returns representation of the producer or errorcode
func readSingleProducer ( node *xmldom.Node ) ( *ProducerVideo, error ) {
    var pr ProducerVideo
    var err error
    pr.Path, err = readSingleProperty ( node, "resource" );
    if err != nil {
        return nil, err
    }
    tmp := strings.Split ( pr.Path,"/")
    pr.Filename = tmp[len(tmp)-1]

    pr.Id = node.GetAttributeValue ( "id" );
    pr.In = node.GetAttributeValue ( "in" );
    pr.Out = node.GetAttributeValue ( "out" );
    pr.InLength, err = readLengthTime ( pr.In );
    if err != nil {
        return nil, err
    }
    pr.OutLength, err = readLengthTime ( pr.Out );
    if err != nil {
        return nil, err
    }

    pr.VideoIndex, err = toInt ( readSingleProperty ( node, "video_index" ) );
    if err != nil {
        return nil, err
    }

    pr.AudioIndex, err = toInt ( readSingleProperty ( node, "audio_index" ) );
    if err != nil {
        return nil, err
    }

    var frNum int
    var frDen int
    frNum, err = toInt ( readSingleProperty ( node, "meta.media.frame_rate_num" ) );
    if err != nil {
        return nil, err
    }
    frDen, err = toInt ( readSingleProperty ( node, "meta.media.frame_rate_den" ) );
    if err != nil {
        return nil, err
    }

    pr.FrameRate = float64 ( frNum ) / float64 ( frDen );

    pr.KdenliveID, err = toInt ( readSingleProperty ( node, "kdenlive:id" ) );
    if err != nil {
        return nil, err
    }

    pr.ClipName, err = readSingleProperty ( node, "kdenlive:clipname" );
    if err != nil {
        return nil, err
    }

    pr.FolderId, err = toInt ( readSingleProperty ( node, "kdenlive:folderid" ) );
    if err != nil {
        return nil, err
    }

    var zones string
    zones, err = readSingleProperty ( node, "kdenlive:clipzones" );
    if err != nil {
        PrintNote ( "No Clipzones; adding clipzones node for video ", pr.ClipName )
        // add missing ClipZones node
        n := node.CreateNode ( "property" );
        n.SetAttributeValue ( "name", "kdenlive:clipzones" );
    } else {
        err = GenericJSONDecode ( strings.NewReader ( zones ), &pr.ClipZones );
        if err != nil {
            return nil, err
        }
    }

    return &pr, nil
}

// find all Videos / producers in the xml
// \param doc the input xml-document
// \return list of all producers (only once!), or error code
func matchVideosToPath ( doc *xmldom.Document ) ([]ProducerVideo, error) {
    var pr []ProducerVideo;
    root := doc.Root;
    nodelist := root.Query ("//producer");

    for _, v := range nodelist {
        pv, err := readSingleProducer ( v )
        if err != nil {
            fmt.Println ("ERROR: ", err );
            continue;
        }
        pv.Node = v;
        pv.FolderName, err = FindFolderName ( doc, pv.FolderId );
        /*if err != nil {
            continue;
        }*/

        if !testVideoInVidList ( pr, pv.ClipName, pv.Path ) {
            pr = append ( pr, *pv );
        }
    }

    return pr, nil
}

// calculate frames from seconds
// \param pr video to use the framerate from
// \param sec input seconds
// \param frms input frame count offset
// \return number of frames representing the secs and frms given the framerate of the video
func secsToFrames ( pr *ProducerVideo, sec uint, frms uint ) uint {
    return uint(pr.FrameRate * float64(sec)) + frms
}

// calculate a frame count from video timestamp
// \param pr video t use the framerate from
// \param vl video timestamp
// \return number of frames representing the timestamp given the framerate of the video
func timeToFrames ( pr *ProducerVideo, vl VideoLength ) uint {
    var secs uint = 0;
    secs += vl.sec;
    secs += vl.min * 60;
    secs += vl.hrs * 60 * 60;
    return secsToFrames ( pr, secs, vl.frs );
}

// create clipzones for single video, but create all zones
// \param pr the video to be processed
// \param zones to be created, representation from the JSON input
// \param offset offset time
func CreateZones ( pr *ProducerVideo, zones []Zone, offset VideoLength ) error {
    var err error = nil
    var err2 error = nil
    var duration int = 30;
    for _, z := range zones {
        var cz ProducerClipzone;
        var In VideoLength
        var Out VideoLength

        // read in an out
        var xs []string = strings.Split(z.Time,"-")
        // Time = 20:30-23:30
        if len(xs) == 2 {
            z.In = xs[0]
            z.Out = xs[1]
        }
        if ( z.In != "" || z.Out != "" ) {
            // In = 20:30
            if ( z.In != "" ) {
                In, err = readLengthTime ( z.In );
            }
            // Out = 23:30
            if (z.Out != "" ) {
                Out, err2 = readLengthTime ( z.Out );
            }
            if err != nil || err2 != nil {
                PrintNote ("Error reading In our Out: ", err, " -- ", err2 );
                // IN ERROR CASE: FALLTHROUGH
            } else {
                if ( z.In == "" ) {
                    cz.In = - timeToFrames ( pr, offset ) + timeToFrames ( pr, Out ) - (uint)(float64(duration)*pr.FrameRate);
                } else {
                    cz.In = - timeToFrames ( pr, offset ) + timeToFrames ( pr, In );
                }

                if ( z.Out == "" ){
                    cz.Out = - timeToFrames ( pr, offset ) + timeToFrames ( pr, In ) + (uint)(float64(duration)*pr.FrameRate);
                } else {
                    cz.Out = - timeToFrames ( pr, offset ) + timeToFrames ( pr, Out );
                }
                cz.Name = z.Name;

                pr.ClipZones = append ( pr.ClipZones, cz );
                continue;
            }
        }

        var before uint = z.Before
        var after uint = z.After
        if z.BeforeTime != "" {
            // time is given and a length BEFORE that time
            vl, err2 := readLengthTime ( z.BeforeTime )
            if err2 != nil {
                PrintErr ("Cannot read BeforeTime of clip ", z.Name, " (", z.BeforeTime, ") err: ", err2 )
                err = err2
            } else {
                before = timeToFrames ( pr, vl );
            }
        }
        if z.AfterTime != "" {
            // time is given and a length AFTER that time
            vl, err2 := readLengthTime ( z.AfterTime )
            if err2 != nil {
            PrintErr ("Cannot read AfterTime of clip ", z.Name, " (", z.AfterTime, ") err: ", err2 )
                err = err2
            } else {
                after = timeToFrames ( pr, vl );
            }
        }

        // apply defaults of clip duration
        if before == 0 {
            before = uint(duration/2);
        }
        if after == 0 {
            after = uint(duration/2);
        }

        time, err2 := readLengthTime ( z.Time );
        // read Timestamp
        if err2 != nil {
            PrintErr ("Cannot read Time of clip ", z.Name, " (", z.Time, ") err: ", err2 )
            err = err2;
            continue
        }

        // use the Timestamp together with the before and after-times specified or the defaults
        cz.In = - timeToFrames ( pr, offset ) + timeToFrames ( pr, time ) - (uint)(float64(before)*pr.FrameRate);
        cz.Out = - timeToFrames ( pr, offset ) + timeToFrames ( pr, time ) + (uint)(float64(after)*pr.FrameRate);
        cz.Name = z.Name;

        pr.ClipZones = append ( pr.ClipZones, cz );
    }
    return err
}

// update the xml-nodes with the created zones
// \param pr the video to be updated
// \return error if any occurred
func UpdateZones ( pr *ProducerVideo ) error  {
    n := pr.Node.QueryOne ( "property[@name='kdenlive:clipzones']")
    if n != nil {
        b, err := json.Marshal ( pr.ClipZones ) 
        if err != nil {
            return err
        }
        n.DontEscape = true
        n.Text = string(b)
        return nil
    }
    return errors.New ("Cannot find clipzones-property")
}

func main () {
    color.Yellow.Println (" ==== KDEnlive Titler ==== ");

    var kdenliveProject string
    var titleDescr string
    var templateID string
    var templateName string
    var o string
    var help bool
    var notitle bool
    flag.StringVar ( &kdenliveProject, "project", "", "Path to the KDEnlive project file" );
    flag.StringVar ( &titleDescr, "descr", "", "Path to the title descriptor file");
    flag.StringVar ( &templateID, "title", "", "ID of the title to be used as template");
    flag.StringVar ( &o, "output", "out.kdenlive", "output file for the new KDEnlive project")
    flag.StringVar ( &templateName, "titlename", "", "name of the template title in the KDEnlive project")
    flag.BoolVar ( &notitle, "notitle", false, "disables title creation");
    flag.BoolVar ( &help, "help", false, "print this help message" );

    var cutEnable bool
    flag.BoolVar ( &cutEnable, "cut", false, "enable precutting videos" );
    flag.Parse();

    if help {
        flag.PrintDefaults();
        os.Exit(0);
    }

    _, err := os.Stat ( titleDescr );
    _, err2 := os.Stat ( kdenliveProject );
    if titleDescr == "" || kdenliveProject == "" ||
       err != nil || err2 != nil {
        Die ("Files not given or not existent, Aborting");
    }

    PrintStep ("Attempting to parse titles file ...");
    t, err := readTitle ( titleDescr );
    if err != nil {
        Die ("Could not parse title descriptor file: ", err);
    }

    PrintStep ("Attempting to parse KDEnlive project file ...");
    doc, err := readKDEnlive ( kdenliveProject );
    if err != nil {
        Die ("Could not parse KDEnlive project file: ", err );
    }

    err = verifyKDEnliveVersion ( doc )
    if err != nil {
        Die ( "Could not verify KDENlive version", err )
    }

    if cutEnable {
        PrintStep ("Cutting stuff, keep your hands to yourselves!")

        // find the video producers in the project bin
        vlist, err := matchVideosToPath ( doc );
        if err != nil {
            Die ("Problem finding videos: ", err );
        }

        // create zones for every video
        var cutlist []Video = t.Video
        for _, c := range cutlist {
            // identify the correct video of the project
            var currentVid *ProducerVideo = nil
            for _, v := range vlist {
                if v.ClipName == c.Name || v.Filename == c.Name {
                    currentVid = &v;
                    break;
                }
            }

            if currentVid == nil {
                PrintErr ( "Video could not be found for clip ", c.Name );
                continue;
            }

            offset, err := readLengthTime ( c.Offset )
            if err != nil && c.Offset != "" {
                Die("Could not read offset string: ", c.Offset, " : ", err );
            }
            err = CreateZones ( currentVid, c.Cutting, offset );
            if err != nil {
                PrintErr ("Video cutting did not work out: ", err )
            }

            err = UpdateZones ( currentVid );
            if err != nil {
                PrintErr ("Updating zones went wrong ", err )
            }
        }

        // TODO place them into the video area
    }

    if notitle == true {
        SkipStep ("Title creation")
    } else {
        PrintStep ("Title Creation");
        SubStep ("Finding the template title")
        var template *xmldom.Node
        if templateID != "" {
            template, err = findTemplateTitle ( doc, templateID )
            if err != nil {
                if templateName != "" {
                    template, err = findTemplateByName ( doc, templateName );
                    if err != nil {
                        Die ("", err );
                    }
                }
            }
        } else if templateName != "" {
            template, err = findTemplateByName ( doc, templateName );
            if err != nil {
                Die ("", err );
            }
        }

        SubStep ("Checking template pattern")
        verifyTemplatePattern ( template, t );

        SubStep ("Creating template titles")
        parent := template.Parent;
        playlist := doc.Root.QueryOne ( "//playlist[@id='main_bin']" )
        var offset int = 0
        for _,region := range t.Region {
            fmt.Println ("Region: ", region.Number, ".", region.Name );
            if len(region.SubRegion) > 0 {
                // iterate over subregions

                for _, sr := range region.SubRegion {
                    fmt.Println ("  SubRegion: ", sr.Number, ".", sr.Name);

                    for _, coll := range sr.Collectable {
                        c := getCollectable ( t, coll.ID )
                        var max int = coll.Max;
                        if c.TotalNumber > 0 {
                            max = c.TotalNumber
                        }

                        if c != nil {
                            fmt.Println ("    ", c.Name, "[", c.Shortcut, "]","(Max:", coll.Max, ", total: ", max, ")" );

                            for n:=1; n<=coll.Max; n++ {
                                newNode := createSubRegionTitleNode (
                                    region,
                                    t.RegionPattern,
                                    sr,
                                    t.SubRegionPattern,
                                    *c,
                                    coll,
                                    n+offset,
                                    max,
                                    template,
                                );

                                // profile node must come first, then my new node, then the rest...
                                parent.Children = insertSlice (parent.Children, newNode, 2 );
                                playlist.Children = append ( playlist.Children, createPlaylistEntry ( newNode ) )
                            }

                            if t.CalculateOffset {
                                offset += coll.Max
                            }
                        }
                    }
                }
            } else {
                // no subregions - just create region-collectable titles
                for _, coll := range region.Collectable {
                    c := getCollectable ( t, coll.ID )
                    var max int = coll.Max;
                    if c.TotalNumber > 0 {
                        max = c.TotalNumber
                    }

                    if c != nil {
                        fmt.Println ("    ", c.Name, "[", c.Shortcut, "]","(Max:", coll.Max, ")" );

                        for n:=1; n<=coll.Max; n++ {
                            newNode := createTitleNode (
                                region,
                                t.RegionPattern,
                                *c,
                                coll,
                                n+offset,
                                max,
                                template,
                            );

                            // profile node must come first, then my new node, then the rest...
                            parent.Children = insertSlice (parent.Children, newNode, 2 );
                            playlist.Children = append ( playlist.Children, createPlaylistEntry ( newNode ) )
                        }

                        if t.CalculateOffset {
                            offset += coll.Max
                        }
                    }
                }
            }
        }
    }

    err = ioutil.WriteFile ( o, []byte(doc.XMLPretty()), 0644 )
    if err != nil {
        Die ("Could not write the output file:", err );
    }

    PrintStep ("Successfully written output file");
    os.Exit(0);
}
