#!/usr/bin/python3
#
import subprocess
import os
import sys

DATAPATH="data/repo"
PROJECTPATH="data/repo"
OUTPATH="out/"

def runCmd ( cmd ):
    '''run a cmd as subprocess'''
    return subprocess.run ( cmd, capture_output=True )

def findFiles ( paths ):
    '''find json and kdenlive files in the datapath'''
    js = []
    kdenlive = []
    for p in paths:
        print ("> Searching", p, " for .json and .kdenlive files")
        for root, dirs, files in os.walk( p ):
            for f in files:
                fs = os.path.join ( root, f )
                if f.endswith(".json") and fs not in js:
                    js.append ( fs )
                if f.endswith(".kdenlive") and fs not in kdenlive:
                    kdenlive.append ( fs )
    return (js, kdenlive);

def runKdenTitler ( datafile, projectfile, templatename, cut, notitle, outfile ):
    '''process the parameters, write the bash command out and run the kdentitler'''
    print ("> Your settings:")
    print ("  > Descriptor:   ", datafile)
    print ("  > Project:      ", projectfile)
    print ("  > Templatename: ", templatename)
    print ("  > Cut Files:    ", "[Y]" if cut else "[n]")
    print ("  > Create Titles:", "[Y]" if not notitle else "[n]")
    print ("  > Outout file:  ", outfile )

    answer = ""
    while answer not in ["Y", "y", "N", "n"]:
        print ("> Correct? [Y/N]: ", end="")
        answer = input("")

    cmd = ["./kdentitler"]
    cmd += [ "--descr", datafile ]
    cmd += [ "--project", projectfile ]
    cmd += [ "--title", templatename ]
    if cut == True :
        cmd += [ "--cut" ]
    if notitle == True :
        cmd += [ "--notitle" ]

    cmd += [ "--output", outfile ]

    ex = datafile.split("/")
    e = ex[-1].split(".")
    o = e[0]+".sh"
    x = ""
    print ("> Command:")
    for c in cmd :
        print ( c, end = " " )
        x += c + " "

    with open ( o, "w+" ) as f:
        f.write ( x )

    if answer.lower() == "n":
        return "abort"

    runCmd ( cmd )

def run ():
    '''gather input files and parameters from the user'''
    datafile = ""
    projectfile = ""
    templatename = "template"
    cut = False
    notitle = False
    outfile = OUTPATH + "out.kdenlive"

    d, p = findFiles ( [DATAPATH, PROJECTPATH] )
    index = 0
    print ("> Found .json descriptors:")
    for di in d:
        print ("  ", index, " - ", di )
        index += 1

    print ("> Which one: ", end="")
    js = int(input(""))

    index = 0
    print ("> Found .kdenlive project files:")
    for pi in p:
        print ("  ", index, " - ", pi )
        index += 1

    print ("> Which one: ", end="")
    kde = int(input(""))

    datafile = d[js]
    projectfile = p[kde]
    print ("> You chose", datafile, " and ", projectfile);
    answer = ""
    while answer not in ["Y", "y", "N", "n"]:
        print ("> Correct? [Y/N]: ", end="")
        answer = input("")

    if answer.lower() == "n":
        return

    print ("Titlename: (default: template)", end="")
    tl = input ("")
    if tl != "":
        titlename = tl

    answer = ""
    while answer not in ["Y", "y", "N", "n"]:
        print ("> Cut? [Y/N]: ", end="")
        answer = input("")

    cut = (answer.lower() == "y")
    answer = ""
    while answer not in ["Y", "y", "N", "n"]:
        print ("> Create titles? [Y/N]: ", end="")
        answer = input("")

    notitle = not (answer.lower() == "y")

    ex = datafile.split("/")
    e = ex[-1].split(".")
    outfile = OUTPATH + e[0]+".kdenlive"
    res = runKdenTitler ( datafile, projectfile, templatename, cut, notitle, outfile )
    if res == "abort":
        return

    if res == "success":
        print ("> Successfully executed")
    else :
        print ( res )

def listFiles( outfile ):
    proc = runCmd ( ["bash", "list.sh"] )
    if proc.returncode != 0:
        return proc.returncode;

    x = proc.stdout.decode('utf-8')
    print ( x )
    with open ( outfile, "w+" ) as f:
        f.write( x );

    return 0;

class Chapter:
    pass
class Collectable:
    def __init__ ( self, name, number, index ):
        self.name = name
        self.number = number
        self.index = index

def collInput () :
    coll = []
    index = 0
    while True:
        index+=1
        name = ""
        number = -1
        print ("Collectablename: ", end="" )
        name = input("")
        if name == "":
            break;

        print ("Number in total: ", end="")
        try:
            number = int(input(""))
        except:
            pass

        coll += [ Collectable(name, number, index) ]
    return coll;

def regionInput ( coll ) :
    result = "{\"Region\": [\n\t"
    index = 0
    while True:
        index += 1
        print ("(%d) Chaptername: "%index, end="")
        name = input("")
        if name == "":
            break
        result += "{\"Name\": \"%s\",\n\t \"Collectable\": [\n"%name

        for c in coll:
            number = -1
            while number < 0:
                print ("\t(%d. %s) Number of %s: "%(index, name, c.name), end="")
                try:
                    number = int(input(""))
                except:
                    pass
            result += "\t\t{\"ID\": %d, \"Max\": %d},\n"%(c.index, number)

        result += "\t]\n\t},"
    result += "}"

    return result

def chapters( outfile ):
    result = ""
    coll = collInput ()

    print ( "Collectables:")
    result += "\"Collectable\": [\n"
    for c in coll:
        result += "\t{ \"ID\": %s, \"Name\": \"%s\", \"Pattern\": \"$coll\", \"Shortcut\": \"%c\", \"TotalNumber\": %d },\n"%( c.index, c.name, c.name.lower()[0], c.number )
        print ( "\t(%d) %s %d"%(c.index, c.name, c.number))

    result += "],\n"

    result += regionInput( coll )

    print ( result)
    with open(outfile, "w+") as f:
        f.write(result)


def main ():
    if len(sys.argv) < 2:
        print ("%s [--run|--list (outfile)|--chapters (outfile)]"%(sys.argv[0]))
        return

    action = sys.argv[1]
    outfile = sys.argv[2] if len(sys.argv) > 2 else "outfile.json"
    if action.endswith("run"):
        return run()
    elif action.endswith("list"):
        return listFiles( outfile )
    elif action.endswith("chapters") or action.endswith("c"):
        return chapters( outfile )

main()
