# KDEntitler

Automatically generate title-clips for an Achivement Guide kind of video for KDEnlive.

You'll need loads of similar but different title-clips, based on regions, maybe subregions, and a counting of collactable instances? I wanted to automate the process of creating these; by writing a more or less simple JSON-file and a template title in KDEnlive, KDEntitler will automatically create new titles into the project file of KDEnlive.

The template title needs to include placeholders for where the text is supposed to go. These are also needed in the JSON-description.

**Note:** kdentitle will check the KDEnlive version of the project file. As a security precaution, it will ask you if you want to proceed, if the version string was not tested before. You will probably be fine proceeding. If everything works, please let me know or send a pull request adding that specific version number to the variable `KDEnliveTestedVersion` in `src/main.go`.

## LICENSE

* The subdirectory xmldom contains a fork of https://github.com/subchen/go-xmldom. It is licensed under APACHE 2.0.
  * I added the possibility to not escape text of a node

## Building

### Requirements

* `golang` Version >= 1.12
* `go get "github.com/gookit/color"`

### Building

```
make build
```

## Titling

### Workflow

1. Write the JSON Description
2. Set up an empty (or filled, should not be a problem) KDEnlive project
    1. Create a template title, place your region, (subregion) and collectable instance placeholders anywhere in the text. Make sure, this is how you want your titles to look; changing them after the fact will be painful.
    2. Save the project.
3. Run kdentitler on the Description and the project
4. Enjoy your new titles! I'm mean... PROFIT!

### Parameters

* -descr string -- Path to the title descriptor file
* -output string -- Output file for the new KDEnlive project (default "out.kdenlive")
* -project string -- Path to the KDEnlive project file
* -title string -- ID of the title to be used as template

The Title Description format (`descr`) is explained later. Make sure `output` and `project` are two different files, if you are not feeling that lucky today.

The `title` parameter denotes the ID of the template title. In the KDEnlive project-file, a title is a producer element, with has property elements; one such property is `kdenlive:id`. Please not the text of that element in `title`.

### Title Creation

The template title is described as escaped XML inside the KDEnlive project. The producer has a property-element with the name "xmldata", which holds as text the escaped XML-data for the title. On that text the values for regions and subregions, as well as collectable instances are **text-replaced**. The XML-description is not parsed (although it would probably be possible).

### Title Description

The title description is a JSON-file. See `src/title.go` for a description of the fields.

```json
{
    "RegionPattern": "$region",
    "SubRegionPattern": "$subregion",
    "Region": [ 
        { "Name": "Tal der Toten",
          "Number": 1,
          "SubRegion": [
              { "Name": "Aufbruch", "Number": 2, "Collectable": [
                  {"ID": 1, "Max": 7 },
                  {"ID": 2, "Max": 1 }
              ]},
              { "Name": "Abbruch", "Number": 3, "Collectable": [
                  {"ID": 1, "Max": 3 },
                  {"ID": 2, "Max": 2 }
              ]}
          ]
        },
        { "Name": "Berg der Lebenden",
          "Number": 3,
          "SubRegion": [
              { "Name": "Durchbruch", "Number": 5, "Collectable": [
                  {"ID": 1, "Max": 0 },
                  {"ID": 2, "Max": 4 }
              ]},
              { "Name": "Leistenbruch", "Number": 8, "Collectable": [
                  {"ID": 1, "Max": 1 },
                  {"ID": 2, "Max": 3 }
              ]}
          ]
        }
    ],
    "Collectable": [
        { "ID": 1, "Name": "Schatz","Pattern": "$coll", "Shortcut": "s" },
        { "ID": 2, "Name": "Tagebuch-Eintrag", "Pattern": "$coll", "Shortcut": "t"}
    ]
}
```

## Cutting Videos

The JSON-description was extended to have a "Video"-member.

```json
"Video": [
    {"Chapter": 1, "Name":"abcd", "Cutting": [
        {
            "Name": "c1",
            "Time": "04:42",
            "BeforeTime": "00:30",
            "AfterTime": "00:30"
        }
    ]}
]
```

A videos with the title `abcd` in the KDEnlive project will be cut into one piece, called `c1` in the project. The clip / zone will be around the 04:42 timestamp, with 30 seconds before and 30 seconds behind.

To enable the cutting-option, use `--cut`
