// this file contains functions for creating DOM nodes and hanging them into
// the KDEnlive project Document. Also: title creation happens here

package main

import (
    "strings"
    "strconv"
    "codeberg.org/snaums/kdentitler/xmldom"
)

// internal global ID for counting producer IDs and kdenlive:ids for the titles
var globalKDEnliveID int = 100;

// create a single DOM node for a single region and collectable instance
// \param region: the descriptor node for a region / chapter
// \param regionPattern: the placeholder pattern for the regions name in the title
// \param c: The collectable information
// \param coll: The collectable information for this region (max)
// \param num: the current number to be produced (num / max)
// \param max: the maximal number to be displayed after the slash
// \param template: the template node from the KDEnlive project file
func createTitleNode (
        region TitleRegion,
        regionPattern string,
        c TitleC,
        coll TitleCollectable,
        num int,
        max int,
        template *xmldom.Node,
) ( *xmldom.Node ) {

    var n *xmldom.Node = deepCopyNode ( template );
    na := attr ( n, "id" );
    na.Value = "producer" + strconv.Itoa(globalKDEnliveID);
    for _,v := range n.Children {
        a := attr ( v, "name" )
        if a != nil {
            if a.Value == "kdenlive:id" {
                v.Text = strconv.Itoa ( globalKDEnliveID );
                globalKDEnliveID++;
            } else if a.Value == "kdenlive:clipname" {
                v.Text = "r" + strconv.Itoa ( region.Number ) + "." + c.Shortcut + strconv.Itoa( num )
            } else if a.Value == "xmldata" {
                txt := v.Text;
                txt = strings.ReplaceAll ( txt, c.Pattern, c.Name + " " + strconv.Itoa(num) + " / " + strconv.Itoa(max) );
                // with and without region / chapter number
                if region.Number > 0 {
                    txt = strings.ReplaceAll ( txt, regionPattern, strconv.Itoa(region.Number) +". "+region.Name );
                } else {
                    txt = strings.ReplaceAll ( txt, regionPattern, region.Name );
                }
                v.Text = txt;
            }
        }
    }
    return n;
}

// create a single DOM node for a single SubRegion and collectable instance
// \param region: the descriptor node for a region / chapter
// \param regionPattern: the placeholder pattern for the regions name in the title
// \param sr: subregion descriptor
// \param srPattern: placeholder patterh for subregions in the title
// \param c: The collectable information
// \param coll: The collectable information for this region (max)
// \param num: the current number to be produced (num / max)
// \param max: the maximal number to be displayed after the slash
// \param template: the template node from the KDEnlive project file
func createSubRegionTitleNode (
        region TitleRegion,
        regionPattern string,
        sr TitleSubRegion,
        srPattern string,
        c TitleC,
        coll TitleCollectable,
        num int,
        max int,
        template *xmldom.Node,
) ( *xmldom.Node ) {

    var n *xmldom.Node = deepCopyNode ( template );
    na := attr ( n, "id" );
    na.Value = "producer" + strconv.Itoa(globalKDEnliveID);
    for _,v := range n.Children {
        a := attr ( v, "name" )
        if a != nil {
            if a.Value == "kdenlive:id" {
                // create a new kdenlive:id
                v.Text = strconv.Itoa ( globalKDEnliveID );
                globalKDEnliveID++;

            } else if a.Value == "kdenlive:clipname" {
                // give it a "catchy" clipname for humans to read
                v.Text = "r" + strconv.Itoa ( region.Number ) + ".sr" + strconv.Itoa ( sr.Number ) + "."+ c.Shortcut + strconv.Itoa( num )

            } else if a.Value == "xmldata" {
                // replace the inside of the title by string replacement!
                txt := v.Text;
                txt = strings.ReplaceAll ( txt, c.Pattern, c.Name + " " + strconv.Itoa(num) + " / " + strconv.Itoa(max) );

                // with and without region / chapter number
                var regionString string
                if region.Number > 0 {
                    regionString = strconv.Itoa(region.Number) +". "+region.Name;
                } else {
                    regionString = region.Name;
                }
                txt = strings.ReplaceAll ( txt, regionPattern, regionString )

                // subregions can have numbers too
                var srString string
                if sr.Number > 0 && region.Number > 0 {
                    srString = strconv.Itoa(region.Number) + "." + strconv.Itoa(sr.Number) + " " + sr.Name;
                } else if sr.Number > 0 && region.Number <= 0 {
                    srString = strconv.Itoa(sr.Number) + ". " + sr.Name
                } else {
                    srString = sr.Name;
                }
                txt = strings.ReplaceAll ( txt, srPattern, srString )
                v.Text = txt;
            }
        }
    }
    return n;
}

// creates a new DOM node for an entry in the KDEnlive project, which links
// to our producer, which we created beforehand (newNode)
func createPlaylistEntry ( newNode *xmldom.Node ) *xmldom.Node {
    var in string
    var out string
    var producerID string

    a := attr ( newNode, "id" );
    if a != nil {
        producerID = a.Value;
    }
    a = attr ( newNode, "in" );
    if a != nil {
        in = a.Value;
    }
    a = attr ( newNode, "out" );
    if a != nil {
        out = a.Value;
    }

    var n xmldom.Node;
    n.Name = "entry";
    n.Text = ""
    n.Attributes = []*xmldom.Attribute{
        &xmldom.Attribute{"producer", producerID},
        &xmldom.Attribute{"in", in},
        &xmldom.Attribute{"out", out},
    }
    return &n;
}
