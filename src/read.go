// this files contains functions for reading the descriptor and project files
// and the verify version and template identification stuff.

package main

import (
    "io"
    "os"
    "fmt"
    "bufio"
    "errors"
	"strings"
    "encoding/json"
    "codeberg.org/snaums/kdentitler/xmldom"
)

// reads the KDEnlive project XML and returns the DOM
func readKDEnlive ( path string ) ( *xmldom.Document, error ) {
    return xmldom.ParseFile ( path );
}

// reads the title descriptor file (JSON) and returns internal struct representation
func readTitle ( path string ) ( *Title, error ) {
    var t Title
    f, err := os.Open ( path );
    if err != nil {
        return nil, err
    }
    defer f.Close();

    err = GenericJSONDecode ( f, &t )
    if err != nil {
        return nil, err
    }

    return &t, err
}

// reads a JSON and uses an interface parameter to fill misc structs
func GenericJSONDecode ( rd io.Reader, pl interface{} ) error {
    dec := json.NewDecoder ( rd );
    for dec.More() {
        err := dec.Decode ( pl );
        if err != nil {
            return err;
        }
    }
    return nil
}

// find the version string in the KDEnlive project DOM and check it against tested ones
func verifyKDEnliveVersion ( doc *xmldom.Document ) ( error ) {
    root := doc.Root
    node := root.QueryOne ("mlt/playlist[@id='main_bin']/property[@name='kdenlive:docproperties.kdenliveversion']");
    if node != nil {
        ver := node.Text;
        fmt.Println ("Found KDEnlive version:", ver)
        for _,v := range KDEnliveTestedVersions {
            if ver == v {
                return nil;
            }
        }
        fmt.Println ("Found version string: '", node.Text,"'; was not tested before");
    } else {
        node = root.QueryOne("//property[@name='kdenlive:docproperties.kdenliveversion']")
        if node != nil {
            ver := node.Text;
            fmt.Println ("Found KDEnlive version:", ver)
            for _,v := range KDEnliveTestedVersions {
                if ver == v {
                    return nil;
                }
            }
            fmt.Println ("Found version string: '", node.Text,"'; was not tested before");
        } else {
            fmt.Println ("Could not determine KDEnlive version number");
        }
    }

    fmt.Println ("Proceed anyway? [y/N]");
    reader := bufio.NewReader(os.Stdin)
    input, _ := reader.ReadString('\n')
    i := string([]byte(input)[0])
    if i == "y" || i =="Y" {
        return nil;
    }
    return errors.New ("Aborted by User");
}

// check if the pattern in the descriptor file can be found in the template title
// only prints messages to stdout, does not exit the program
func verifyTemplatePattern ( n *xmldom.Node, t *Title ) {
    if n == nil || t == nil {
        return
    }

    txt := ""
    for _, v := range n.Children {
        a := attr ( v, "name" )
        if a.Value == "xmldata" {
            txt = v.Text;
            break;
        }
    }

    if txt == "" {
        PrintErr ("Empty template title");
    }

    if strings.Contains ( txt, t.RegionPattern ) == false {
        PrintNote ( "Could not find Region Pattern '"+t.RegionPattern+"' in Template title" );
    }
    if strings.Contains ( txt, t.SubRegionPattern ) == false {
        PrintNote ( "Could not find SubRegion Pattern '"+t.SubRegionPattern+"' in Template title" );
    }

    for _, c := range t.Collectable {
        if strings.Contains ( txt, c.Pattern ) == false {
            PrintNote ( "Could not find Collectable ('"+c.Name+"') Pattern '"+c.Pattern+"' in Template title" );
        }
    }
}

// identify a template title by its kdenlive:id-property
func findTemplateTitle ( doc *xmldom.Document, templateID string ) ( *xmldom.Node, error ) {
    root := doc.Root
    nodelist := root.Query ( "producer" );
    for _,v := range nodelist {
        id := v.QueryOne ( "property[@name='kdenlive:id']" );
        if id.Text == templateID {
            return v, nil;
        }
    }
    return nil, errors.New ("Could not find producer with id ("+templateID+")" );
}

func findTemplateByName ( doc *xmldom.Document, name string ) ( *xmldom.Node, error ) {
    root := doc.Root
    nodelist := root.Query ( "producer" );
    fmt.Printf ("NL: %+v\n", nodelist )
    for _, v := range nodelist {
        n := v.QueryOne ("property[@name='kdenlive:clipname']")
        if n != nil {
            if n.Text == name {
                return v, nil
            }
        }
    }
    return nil, errors.New ("Could not find producer by its name ("+name+")");
}
