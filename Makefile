all: kdentitler
	./run.py --run

kdentitler: build

build:
	go build -o kdentitler src/*.go

clean:
	rm kdentitler

dep:
	go get github.com/gookit/color github.com/antchfx/xpath

ctags:
	ctags -R src

dist: build
	-mkdir dist
	mv kdentitler dist/
