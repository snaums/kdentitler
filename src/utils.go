package main

import (
	"os"
	"strconv"
    "codeberg.org/snaums/kdentitler/xmldom"
	"github.com/gookit/color"
)


var number int = 0;
var subnumber int = 0;
func PrintStep ( txt string, vargs ...interface{} ) {
    number++;
    subnumber = 0;
    color.Green.Print ( strconv.Itoa(number) + ". " + txt )
    color.Green.Println ( vargs... )
}

func SubStep ( txt string, vargs ...interface{} ) {
    subnumber++;
    color.Green.Print( "  " + strconv.Itoa(subnumber) + ". " + txt )
    color.Green.Println ( vargs... )
}

func SkipStep ( txt string, vargs ...interface{} ) {
    number++;
    subnumber = 0;
    color.Blue.Print ( strconv.Itoa(number) + ". SKIP: " + txt )
    color.Blue.Println ( vargs... )
}

func SkipSubStep ( txt string, vargs ...interface{} ) {
    subnumber++;
    color.Blue.Print( "  " + strconv.Itoa(subnumber) + ". SKIP: " + txt )
    color.Blue.Println ( vargs... )
}

func PrintNote ( txt string, vargs ...interface{} ) {
    color.Yellow.Print ( "//" + txt )
    color.Yellow.Println ( vargs...)
}

func PrintErr ( txt string, vargs ...interface{} ) {
    color.Red.Print ( "!!" + txt )
    color.Red.Println ( vargs...)
}

func Die ( txt string, vargs ...interface{} ) {
    PrintErr ( txt, vargs )
    os.Exit(-1);
}


// return the global collectable struct for a given id
func getCollectable ( t *Title, id int ) ( *TitleC ) {
    for _, v := range t.Collectable {
        if v.ID == id {
            return &v;
        }
    }
    return nil;
}

// return the *xmldom.Attribute of a node (n) with a given name (attrName)
func attr ( n *xmldom.Node, attrName string ) ( *xmldom.Attribute ) {
    for _, v := range n.Attributes {
        if v.Name == attrName {
            return v;
        }
    }
    return nil
}

// inserts a single node into a slice of nodes at a given index (starts counting at 0)
func insertSlice ( x []*xmldom.Node, i *xmldom.Node, index int ) []*xmldom.Node {
    return append(x[:index], append([]*xmldom.Node{i}, x[index:]...)...)
}

// deep copy a node (copy it's children and the node itself), so it can be 
// modified later on
func deepCopyNode ( i *xmldom.Node ) ( *xmldom.Node ) {
    var o xmldom.Node
    // don't copy parent or document
    o.Parent = i.Parent
    o.Document = i.Document
    o.Name = i.Name
    o.Text = i.Text
    // deep copy children
    for _, c := range i.Children {
        o.Children = append ( o.Children, deepCopyNode ( c ));
    }

    // deep copy attributes
    for _, a := range i.Attributes {
        o.Attributes = append ( o.Attributes, &xmldom.Attribute{Name: a.Name, Value: a.Value} );
    }

    return &o;
}

