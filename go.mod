module codeberg.org/snaums/kdentitler

go 1.16

require (
	github.com/antchfx/xpath v1.2.0
	github.com/gookit/color v1.4.2
)
